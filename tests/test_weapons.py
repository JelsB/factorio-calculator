import pytest
from collections import namedtuple

from factpy.weapons import UraniumRoundsMagazine
from factpy.centrifuge import Centrifuge
from factpy.assembling_machines import AssemblingMachine1
from factpy.belts import YellowBelt

all_weapons = [UraniumRoundsMagazine]
Factories = namedtuple('Factories', ['assembling_machine', 'centrifuge'])


@pytest.mark.parametrize('product', all_weapons)
def test_ingredient_factories(product):
    # checks if no unexpected errors

    bus_belt = YellowBelt
    factories = Factories(AssemblingMachine1, Centrifuge)

    product.number_of_ingredient_factories(factories, bus_belt)
