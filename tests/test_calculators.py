import pytest
import re
from factpy.calculators import (OilProcessingCalculator, FactoryCalculator,
                                SmeltingCalculator)
from factpy.material_types import AdvancedOilProcessingRecipe
from factpy.oil_processing import Petroleum
from factpy.nuclear import Uranium235, Uranium238, NuclearFuel
from factpy.weapons import UraniumRoundsMagazine
from factpy.plates import IronPlate

nuclear_products = [Uranium235, Uranium238, NuclearFuel]


def test_oil_processing_calculator():
    wanted_material = Petroleum
    desired_ouput = 19.5
    recipe_type = AdvancedOilProcessingRecipe

    calc = OilProcessingCalculator(wanted_material, recipe_type)

    production_line = calc.calc_production_line_from_desired_output(desired_ouput)
    str = calc.printable_production_line(desired_ouput, production_line)
    print(str)
    assert re.search(fr'19.5 Petroleum.*{recipe_type}.*optimal', str)
    assert re.search('total of 20.0 Crude Oil', str)
    assert re.search('total of 26.5 Water', str)

    assert re.search(r'total of 1\.0 Oil Refinery.*Advanced Oil Processing', str)
    assert re.search('-- needs 20.0 Crude Oil', str)
    assert re.search('-- needs 10.0 Water', str)
    assert re.search(r'\+\+ produces 11.0 Petroleum', str)
    assert re.search(r'\+\+ produces 9.0 Light Oil', str)
    assert re.search(r'\+\+ produces 5.0 Heavy Oil', str)

    assert re.search(r'total of 0\.85 Chemical Plant.*Light Oil Cracking', str)
    assert re.search('-- needs 12.75 Light Oil', str)
    assert re.search('-- needs 12.75 Water', str)
    assert re.search(r'\+\+ produces 8.5 Petroleum', str)

    assert re.search(r'total of 0\.25 Chemical Plant.*Heavy Oil Cracking', str)
    assert re.search('-- needs 5.0 Heavy Oil', str)
    assert re.search('-- needs 3.75 Water', str)
    assert re.search(r'\+\+ produces 3.75 Light Oil', str)


def test_printable_oil_production_liner():
    wanted_material = Petroleum
    desired_ouput = 19.5
    recipe_type = AdvancedOilProcessingRecipe

    calc = OilProcessingCalculator(wanted_material, recipe_type)

    assert calc.calc_production_line_from_desired_output(desired_ouput)


@pytest.mark.parametrize('product', nuclear_products)
def test_nuclear_processing(product):
    wanted_material = product
    wanted_number_of_output_per_second = 1

    calc = FactoryCalculator(wanted_material)

    assert calc.calc_production_line_from_desired_output(
        wanted_number_of_output_per_second)


@pytest.mark.parametrize('product', [UraniumRoundsMagazine])
def test_mix_assembled_nuclear_products(product):
    wanted_material = product
    wanted_number_of_output_per_second = 1

    calc = FactoryCalculator(wanted_material)

    assert calc.calc_production_line_from_desired_output(
        wanted_number_of_output_per_second)


def test_printable_factory_production_line():
    wanted_material = UraniumRoundsMagazine
    wanted_number_of_output_per_second = 1
    calc = FactoryCalculator(wanted_material)

    production_line = calc.calc_production_line_from_desired_output(
        wanted_number_of_output_per_second)
    str = calc.printable_production_line(production_line)

    assert re.search(r'20\.0 Uranium Rounds Magazine Assembling Machine1.*1.0 units', str)
    assert re.search(r'6\.00.* Piercing Rounds Magazine Assembling Machine1', str)
    assert re.search(r'2\.0 .* Fire Arm Magazine Assembling Machine1', str)
    assert re.search(r'24\.1691.* Uranium238 Centrifuge', str)
    assert re.search(r'0\.333.* Yellow Belt .* Copper Plate .* bus', str)
    assert re.search(r'0\.2666.* Yellow Belt .* Iron Plate .* bus', str)
    assert re.search(r'0\.0666.* Yellow Belt .* Steel Plate .* bus', str)
    assert re.search(r'0\.67.* Yellow Belt .* Uranium Ore .* bus', str)


@pytest.mark.parametrize('output_unit', ['belt', 'items'])
def test_smelting_calculator(output_unit):
    wanted_material = IronPlate
    # this will be belts and items per second
    wanted_output_number = 5

    calc = SmeltingCalculator(wanted_material, output_unit)

    assert calc.calc_production_line_from_desired_belts(wanted_output_number)


@pytest.mark.parametrize('output_unit', ['belt', 'items'])
def test_printable_smelting_production_line(output_unit):
    wanted_material = IronPlate
    # this will be belts and items per second
    wanted_output_number = 2

    calc = SmeltingCalculator(wanted_material, output_unit)

    production_line = calc.calc_production_line_from_desired_belts(wanted_output_number)
    str = calc.printable_production_line(wanted_output_number, production_line)

    if output_unit == 'belt':
        assert re.search(r'96\.0 Iron Plate Stone Furnace.*2 Yellow Belt of Iron Plate', str)
    if output_unit == 'items':
        assert re.search(r'96\.0 Iron Plate Stone Furnace.*2 Iron Plate', str)
    assert re.search(r'2\.0 Yellow Belt.*Iron Ore', str)
    assert re.search(r'0\.14.* Yellow Belt.*Coal', str)
