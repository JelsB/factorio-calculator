# For some reason Kite can't index factpy modeules properly unless
# these modules are imported explicitly
# import factpy.oil_processing
# import factpy.calculators
# Real needed imports
import pytest
from factpy.oil_processing import (Petroleum, LightOil, BasicOilProcessing,
                                   AdvancedOilProcessing, CoalLiquefaction,
                                   LightOilCracking, HeavyOilCracking, HeavyOil)
from factpy.natural_resources import CrudeOil, Water, Coal
from factpy.crafting_components import Steam
from factpy.material_types import AdvancedOilProcessingRecipe
from factpy.oil_utils import RecursiveOilRecipe


@pytest.mark.parametrize("recipe, expected", [(BasicOilProcessing, 9),
                                              (AdvancedOilProcessing, 11),
                                              (LightOilCracking, 10),
                                              (CoalLiquefaction, 2)])
def test_petroleum_production_rate_with_correct_recipes(recipe, expected):
    assert Petroleum.units_per_second(recipe) == expected


@pytest.mark.parametrize('recipe', [HeavyOilCracking])
def test_petrolem_production_rate_with_incorrect_recipes(recipe):
    with pytest.raises(KeyError):
        Petroleum.units_per_second(recipe)


@pytest.mark.parametrize("recipe, expected", [(AdvancedOilProcessing, 9),
                                              (HeavyOilCracking, 15),
                                              (CoalLiquefaction, 4)])
def test_light_oil_production_rate_with_correct_recipes(recipe, expected):
    assert LightOil.units_per_second(recipe) == expected


@pytest.mark.parametrize('recipe', [BasicOilProcessing, LightOilCracking])
def test_light_oil_production_rate_with_incorrect_recipes(recipe):
    with pytest.raises(KeyError):
        LightOil.units_per_second(recipe)


@pytest.mark.parametrize("recipe, expected", [(AdvancedOilProcessing, 5),
                                              (CoalLiquefaction, 18)])
def test_heavy_oil_production_rate_with_correct_recipes(recipe, expected):
    assert HeavyOil.units_per_second(recipe) == expected


@pytest.mark.parametrize('recipe', [BasicOilProcessing, LightOilCracking,
                                    HeavyOilCracking])
def test_heavy_oil_production_rate_with_incorrect_recipes(recipe):
    with pytest.raises(KeyError):
        HeavyOil.units_per_second(recipe)

#


@pytest.mark.parametrize('ingredient, recipe, expected',
                         [(CrudeOil, BasicOilProcessing, 20),
                          (CrudeOil, AdvancedOilProcessing, 20),
                          (Water, AdvancedOilProcessing, 10),
                          (LightOil, LightOilCracking, 15),
                          (Water, LightOilCracking, 15),
                          (Coal, CoalLiquefaction, 2),
                          (HeavyOil, CoalLiquefaction, 5),
                          (Steam, CoalLiquefaction, 10), ])
def test_petroleum_needed_ingredient_rate_with_correct_recipe(ingredient,
                                                              recipe, expected):
    ingredients = Petroleum.ingredient_units_needed_per_second(ingredient,
                                                               recipe)
    assert ingredients == expected


def test_petroleum_production_with_recursive_conversions():
    # Arrange
    one_light_oil_cracking = RecursiveOilRecipe({LightOil: 15, Water: 15},
                                                {Petroleum: 10},
                                                {LightOilCracking: 1})

    one_advanced_oil_processing = RecursiveOilRecipe({CrudeOil: 20.0, Water: 10.0},
                                                     {Petroleum: 11.0,
                                                         LightOil: 9.0, HeavyOil: 5.0},
                                                     {AdvancedOilProcessing: 1})
    # additionally to one advanced oil processing
    plus_light_oil_cracking = RecursiveOilRecipe({CrudeOil: 20.0, Water: 19.0},
                                                 {Petroleum: 17.0, HeavyOil: 5.0},
                                                 {LightOilCracking: 0.6, AdvancedOilProcessing: 1})
    # additionally to one advanced oil processing
    plus_heavy_oil_cracking = RecursiveOilRecipe({CrudeOil: 20.0, Water: 13.75},
                                                 {LightOil: 12.75, Petroleum: 11.0},
                                                 {HeavyOilCracking: 0.25, AdvancedOilProcessing: 1})
    # additionally to one advanced oil processing
    plus_light_and_heavy_oil_cracking = RecursiveOilRecipe({Water: 22.75, CrudeOil: 20.0},

                                                           {Petroleum: 17.0,
                                                               LightOil: 3.75},
                                                           {LightOilCracking: 0.6, HeavyOilCracking: 0.25, AdvancedOilProcessing: 1})
    # additionally to one advanced oil processing
    plus_recursive_light_and_heavy_oil_cracking = RecursiveOilRecipe({Water: 26.5, CrudeOil: 20.0},
                                                                     {Petroleum: 19.5},
                                                                     {LightOilCracking: 0.85, HeavyOilCracking: 0.25, AdvancedOilProcessing: 1})

    # Act
    all_solutions = Petroleum.get_productions_recipes_with_recursive_conversion()

    # Assert
    assert len(all_solutions) == 6
    assert one_light_oil_cracking in all_solutions
    assert one_advanced_oil_processing in all_solutions
    assert plus_light_oil_cracking in all_solutions
    assert plus_heavy_oil_cracking in all_solutions
    assert plus_light_and_heavy_oil_cracking in all_solutions
    assert plus_recursive_light_and_heavy_oil_cracking in all_solutions


def test_level_property_recursive_oil_recipe():

    recipe = RecursiveOilRecipe({LightOil: 0, Water: 0},
                                {Petroleum: 0},
                                {LightOilCracking: 0})

    assert recipe.level == 1


def test_dynamic_level_property_recursive_oil_recipe():
    # recursive recipe with level 1
    recipe = RecursiveOilRecipe({LightOil: 0, Water: 0},
                                {Petroleum: 0},
                                {LightOilCracking: 0})

    # level should update to 0
    recipe.recipes = {BasicOilProcessing: 0}

    assert recipe.level == 0


def test_ordered_list_of_recursive_oil_recipes():
    # Arrange
    one_light_oil_cracking = RecursiveOilRecipe({LightOil: 15, Water: 15},
                                                {Petroleum: 10},
                                                {LightOilCracking: 1})

    one_advanced_oil_processing = RecursiveOilRecipe({CrudeOil: 20.0, Water: 10.0},
                                                     {Petroleum: 11.0,
                                                         LightOil: 9.0, HeavyOil: 5.0},
                                                     {AdvancedOilProcessing: 1})
    # additionally to one advanced oil processing
    plus_light_oil_cracking = RecursiveOilRecipe({CrudeOil: 20.0, Water: 19.0},
                                                 {Petroleum: 17.0, HeavyOil: 5.0},
                                                 {LightOilCracking: 0.6, AdvancedOilProcessing: 1})
    # additionally to one advanced oil processing
    plus_heavy_oil_cracking = RecursiveOilRecipe({CrudeOil: 20.0, Water: 13.75},
                                                 {LightOil: 12.75, Petroleum: 11.0},
                                                 {HeavyOilCracking: 0.25, AdvancedOilProcessing: 1})
    # additionally to one advanced oil processing
    plus_light_and_heavy_oil_cracking = RecursiveOilRecipe({Water: 22.75, CrudeOil: 20.0},
                                                           {Petroleum: 17.0,
                                                               LightOil: 3.75},
                                                           {LightOilCracking: 0.6, HeavyOilCracking: 0.25, AdvancedOilProcessing: 1})
    # additionally to one advanced oil processing
    plus_recursive_light_and_heavy_oil_cracking = RecursiveOilRecipe({Water: 26.5, CrudeOil: 20.0},
                                                                     {Petroleum: 19.5},
                                                                     {LightOilCracking: 0.85, HeavyOilCracking: 0.25, AdvancedOilProcessing: 1})

    expected_list = [plus_recursive_light_and_heavy_oil_cracking,  # 19
                     plus_light_and_heavy_oil_cracking,  # 17
                     plus_light_oil_cracking,  # 17
                     plus_heavy_oil_cracking,  # 11
                     one_advanced_oil_processing,  # 11
                     one_light_oil_cracking,  # 10
                     ]

    option1 = expected_list[:]
    option1[1], option1[2] = option1[2], option1[1]

    option2 = expected_list[:]
    option2[3], option2[4] = option2[4], option2[3]

    option3 = option2[:]
    option3[1], option3[2] = option3[2], option3[1]

    # Act
    ordered_list = Petroleum.get_recursive_recipes(AdvancedOilProcessingRecipe)

    # Assert
    assert ordered_list == option1 or ordered_list == option2 or ordered_list == option3


def test_scale_recursive_oil_recipe():
    scale_value = 2
    rr = RecursiveOilRecipe({Water: 1, CrudeOil: 1}, {Petroleum: 1.0},
                            {LightOilCracking: 0.5, HeavyOilCracking: 0.25, AdvancedOilProcessing: 1})

    scaled_rr = RecursiveOilRecipe({Water: 1*scale_value, CrudeOil: 1*scale_value},
                                   {Petroleum: 1.0*scale_value},
                                   {LightOilCracking: 0.5*scale_value, HeavyOilCracking: 0.25*scale_value, AdvancedOilProcessing: 1*scale_value})
    rr.scale(scale_value)

    assert rr == scaled_rr
