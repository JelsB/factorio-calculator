import pytest
from collections import namedtuple


from factpy.sciences import (RedScience, GreenScience, BlackScience,
                             BlueScience, PurpleScience, GoldScience)
from factpy.belts import YellowBelt
from factpy.assembling_machines import AssemblingMachine1
from factpy.centrifuge import Centrifuge

all_sciences = [RedScience, GreenScience, BlackScience, BlueScience,
                PurpleScience, GoldScience]
Factories = namedtuple('Factories', ['assembling_machine', 'centrifuge'])


@pytest.mark.parametrize('science', all_sciences)
def test_sciences(science):
    # checks if no unexpected errors

    assembling_machine = AssemblingMachine1
    bus_belt = YellowBelt

    factories = Factories(AssemblingMachine1, Centrifuge)

    science.number_of_ingredient_factories(factories, bus_belt)
