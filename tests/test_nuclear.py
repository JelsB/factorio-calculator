import pytest
from collections import namedtuple

from factpy.nuclear import Uranium235, Uranium238, NuclearFuel
from factpy.centrifuge import Centrifuge
from factpy.natural_resources import UraniumOre
from factpy.belts import YellowBelt
from factpy.assembling_machines import AssemblingMachine1


uranium_types = [Uranium235, Uranium238]
nuclear_products = uranium_types[:]
nuclear_products.append(NuclearFuel)

Factories = namedtuple('Factories', ['assembling_machine', 'centrifuge'])


@pytest.mark.parametrize('uranium', uranium_types)
def test_uranium_ingrendients_per_second(uranium):
    needed = 10
    crafting_time = 12

    units = uranium.ingredient_units_needed_per_second(UraniumOre, Centrifuge)
    assert units == needed/crafting_time


@pytest.mark.parametrize('product', nuclear_products)
def test_ingredient_factories(product):
    # checks if no unexpected errors

    bus_belt = YellowBelt
    factories = Factories(AssemblingMachine1, Centrifuge)

    product.number_of_ingredient_factories(factories, bus_belt)
