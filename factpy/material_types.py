from factpy.bus import BusMaterial, LiquidOilBusMaterial
from collections import Counter
import re
import itertools
from copy import deepcopy
from factpy.oil_utils import RecursiveOilRecipe


class Meta(type):

    def __repr__(self):
        # split string based on CamelCase
        return re.sub(r'([A-Z])', r' \1', self.__name__).strip()


class OilMeta(Meta):
    '''Meta class for Oil to allow comparison of classes'''

    def __lt__(self, other):
        return self.level < other.level

    def __le__(self, other):
        return self.level <= other.level

    def __eq__(self, other):
        return self.level == other.level

    def __ne__(self, other):
        return not self.__eq__(other)

    def __gt__(self, other):
        return self.__lt__(other, self)

    def __ge__(self, other):
        return self.__le__(other, self)

    def __hash__(self):
        return super().__hash__()


class FactoryMaterial(metaclass=Meta):
    """Material produced in a factory."""

    @classmethod
    def crafing_time_with_factory(cls, factory):
        return cls.crafting_time / factory.crafting_speed

    @classmethod
    def units_per_second(cls, factory):
        return cls.output / cls.crafing_time_with_factory(factory)

    @classmethod
    def ingredient_units_needed_per_second(cls, ingredient, factory):
        return cls.units_per_second(factory) * cls.ingredients[ingredient] / cls.output

    @classmethod
    def number_of_ingredient_factories(cls, factories, belt, desired_number=1):
        # Counter can handle floats but certain method will not work e.g. elements()
        factory_cnt = Counter()
        bus_belt_cnt = Counter()
        bus_pipe_cnt = Counter()

        for ingredient in cls.ingredients.keys():
            if issubclass(ingredient, NuclearMaterial):
                factory = factories.centrifuge
            else:
                factory = factories.assembling_machine

            needed_ingredients = (cls.ingredient_units_needed_per_second(ingredient, factory)
                                  * desired_number)

            if issubclass(ingredient, BusMaterial):
                if issubclass(ingredient, LiquidOilBusMaterial):
                    bus_pipe_cnt[ingredient] = needed_ingredients
                else:
                    bus_belt_cnt[ingredient] = needed_ingredients / belt.capacity
                continue

            number = needed_ingredients / ingredient.units_per_second(factory)

            factory_cnt[ingredient] = number

            # Recursively call the same method on the ingredient
            (rec_factory_cnt,
             rec_bus_belt_cnt,
             rec_bus_pipe_cnt) = ingredient.number_of_ingredient_factories(
                factories, belt, number)

            # Update couters with recursive counters
            factory_cnt = factory_cnt + rec_factory_cnt
            bus_belt_cnt = bus_belt_cnt + rec_bus_belt_cnt
            bus_pipe_cnt = bus_pipe_cnt + rec_bus_pipe_cnt

        return (factory_cnt, bus_belt_cnt, bus_pipe_cnt)


class AssembledMaterial(FactoryMaterial):
    """docstring forAssembledMaterial."""


class NuclearMaterial(FactoryMaterial):
    """Material produced in a centrifuge."""


class SmeltedMaterial(metaclass=Meta):
    """docstring fo SmeltedMaterial"""

    @classmethod
    def crafing_time_with_furnace(cls, furnace):
        return cls.crafting_time / furnace.crafting_speed

    @classmethod
    def units_per_second(cls, furnace):
        return cls.output / cls.crafing_time_with_furnace(furnace)

    @classmethod
    def furnaces_per_x_belt(cls, x, belt, furnace):
        return x * belt.capacity / cls.units_per_second(furnace)

    @classmethod
    def ratio_ingredient_output(cls):
        return {key: val/cls.output for key, val in cls.ingredients.items()}


class MinedMaterial(metaclass=Meta):
    """docstring fo SmeltedMaterial"""


class OilProcessingRecipe(metaclass=OilMeta):
    """docstring for OilProcessingRecipe."""

    def __init__(self):
        super(OilProcessingRecipe, self).__init__()

    @classmethod
    def units_per_second(cls, material):
        try:
            return cls.output[material] / cls.crafting_time
        except KeyError as e:
            print(f'The recipe {cls} cannot be used for producing {material}')
            raise

    @classmethod
    def ingredient_units_needed_per_second(cls, ingredient):
        try:
            return cls.ingredients[ingredient] / cls.crafting_time
        except KeyError as e:
            print(f'The recipe {cls} does not need ingredient {ingredient}')
            raise

    @classmethod
    def all_ingredient_units_needed_per_second(cls):
        return {ingredient: cls.ingredient_units_needed_per_second(ingredient)
                for ingredient in cls.ingredients.keys()}

    @classmethod
    def all_output_units_per_second(cls):
        return {output: cls.units_per_second(output)
                for output in cls.output.keys()}


class BasicOilProcessingRecipe(OilProcessingRecipe):
    """docstring for BasicOilProcessingRecipe."""
    level = 0

    def __init__(self):
        super().__init__()


class AdvancedOilProcessingRecipe(OilProcessingRecipe):
    """docstring for BasicOilProcessingRecipe."""
    level = 1

    def __init__(self):
        super().__init__()


class CoalOilProcessingRecipe(OilProcessingRecipe):
    """docstring for BasicOilProcessingRecipe."""
    level = 2

    def __init__(self):
        super().__init__()


class OilProccessedMaterial(metaclass=OilMeta):
    """docstring fo SmeltedMaterial"""

    @classmethod
    def units_per_second(cls, recipe):
        return recipe.units_per_second(cls)

    @classmethod
    def ingredient_units_needed_per_second(cls, ingredient, recipe):
        return recipe.ingredient_units_needed_per_second(ingredient)

    @classmethod
    def all_production_recipes(cls):
        return cls.get_production_recipe(OilProcessingRecipe)

    @classmethod
    def all_consumption_recipes(cls):
        return cls.get_consumptions_recipe(OilProcessingRecipe)

    @classmethod
    def get_consumptions_recipe(cls, recipe_type):
        return [recipe for recipe in recipe_type.__subclasses__()
                if cls in recipe.ingredients]

    @classmethod
    def get_production_recipe(cls, recipe_type):
        return [recipe for recipe in recipe_type.__subclasses__()
                if cls in recipe.output]

    @classmethod
    def get_recursive_recipes(cls, recipe_type):
        recipes = cls.get_productions_recipes_with_recursive_conversion(
            recipe_type=recipe_type)

        # Sort by descending production value of class
        recipes.sort(key=lambda recipe: recipe.output[cls], reverse=True)

        return recipes

    @classmethod
    def get_productions_recipes_with_recursive_conversion(cls,
                                                          recipe_type=AdvancedOilProcessingRecipe,
                                                          end_material=None):
        # By default, perform recursion conversion to production material
        if end_material is None:
            end_material = cls

        all_converted_solutions = []

        # TODO: What is looped over all recipes?
        for recipe in cls.get_production_recipe(recipe_type):
            input = recipe.all_ingredient_units_needed_per_second()
            output = recipe.all_output_units_per_second()
            recipes = {recipe: 1}

            recursive_recipe = RecursiveOilRecipe(input, output, recipes)
            solutions = [recursive_recipe]

            all_converted_solutions.extend(
                cls.convert_if_possible(end_material, recursive_recipe, solutions))

        return all_converted_solutions

    @classmethod
    def convert_if_possible(cls, end_material, recursive_recipe, converted_combinations=[]):

        able_to_convert = cls.get_convertable_materials(
            recursive_recipe.output, end_material)
        all_combinations = cls.get_conversion_combinations(able_to_convert)

        for combination in all_combinations:
            originale_recursive_recipe = deepcopy(recursive_recipe)

            converted_recursive_recipe = cls.apply_conversions(combination,
                                                               originale_recursive_recipe)

            converted_combinations.append(converted_recursive_recipe)

            # Recursively convert the most frequently converted output
            # Recursive conversion of single combinations is already done
            # when combining conversion combinations
            if len(combination) > 1:
                cls.convert_if_possible(end_material, converted_recursive_recipe,
                                        converted_combinations)

        return converted_combinations

    @staticmethod
    def rescale_consumption_recipe(desired_amount, material, recipe):
        desired_ratio = (desired_amount /
                         recipe.ingredient_units_needed_per_second(material))
        input = Counter({key: value * desired_ratio for key, value in
                         recipe.all_ingredient_units_needed_per_second().items()})
        output = Counter({key: value * desired_ratio for key, value in
                          recipe.all_output_units_per_second().items()})

        return desired_ratio, input, output

    @staticmethod
    def get_convertable_materials(counter_of_materials, end_material):
        able_to_convert = []
        for matertial, value in counter_of_materials.items():
            if matertial > end_material:
                able_to_convert.append({matertial: value})

        return able_to_convert

    @staticmethod
    def get_conversion_combinations(able_to_convert):
        """"
        able_to_convert = [1,2]
        get_conversion_combinations(able_to_convert)
        >>> [[1], [2], [1,2]]
        """
        all_combinations = []
        for r in range(1, len(able_to_convert) + 1):
            combinations_object = itertools.combinations(able_to_convert, r)
            for i in list(combinations_object):
                all_combinations.append(list(i))

        return all_combinations

    @classmethod
    def apply_conversions(cls, list_of_conversions, recursive_recipe):
        '''
        list_of_conversions examples:
        - [{Light Oil: 9.0}]
        - [{Heavy Oil: 5.0}]
        - [{Light Oil: 9.0}, {Heavy Oil: 5.0}]
        '''
        for material_dic in list_of_conversions:
            for material, amount in material_dic.items():
                # TODO: This harc coding will have to disappear when adding CoalLiquefaction
                consumption_recipes = material.get_consumptions_recipe(
                    AdvancedOilProcessingRecipe)
                # NOTE: This recipe loop is currently just over one object
                for recipe in consumption_recipes:
                    (comb_ratio,
                     comb_input,
                     comb_output) = cls.rescale_consumption_recipe(amount,
                                                                   material,
                                                                   recipe)
                    to_convert = Counter({material: amount})
                    used_recipe = Counter({recipe: comb_ratio})

                    # update recipes, input, and ouput after conversion
                    recursive_recipe.recipes = recursive_recipe.recipes + used_recipe
                    recursive_recipe.input = recursive_recipe.input + comb_input - to_convert
                    recursive_recipe.output = recursive_recipe.output + comb_output - to_convert

        return recursive_recipe
