from factpy.belts import YellowBelt
from factpy.assembling_machines import AssemblingMachine1
from factpy.furnaces import StoneFurnace
from factpy.natural_resources import Coal
from factpy.material_types import NuclearMaterial
from factpy.centrifuge import Centrifuge
from collections import namedtuple

Factories = namedtuple('Factories', ['assembling_machine', 'centrifuge'])
ProductionLine = namedtuple('ProductionLine',
                            ['main_factory', 'ingredient_factories',
                             'bus_belts', 'bus_pipes'])

SmeltingProductionLine = namedtuple('SmeltingProductionLine',
                                    ['furnaces', 'ingredient_belts',
                                     'fuel_belts'])
OilProductionLine = namedtuple('OilProductionLine',
                               ['recipe', 'base_ingredient_recipes',
                                'base_output_recipes'])


class FactoryCalculator():
    """docstring for FactoryCalculator."""

    def __init__(self, material, assembling_machine=AssemblingMachine1,
                 bus_belt=YellowBelt):

        super(FactoryCalculator, self).__init__()
        self.material = material
        self.assembling_machine = assembling_machine
        self.bus_belt = bus_belt
        self.produced_output_per_factory = self.produced_output_per_second()
        self.factories = Factories(self.assembling_machine, Centrifuge)

    @property
    def main_factory(self):
        factory = self.assembling_machine
        if issubclass(self.material, NuclearMaterial):
            factory = Centrifuge
        return factory

    def produced_output_per_second(self, number_of_factories=1):
        return (self.material.units_per_second(self.main_factory)
                * number_of_factories)

    def calc_production_line_from_desired_factories(self, desired_factories):
        return self.calc_production_line(desired_factories)

    def calc_production_line_from_desired_output(self, desired_output):
        needed_factories = desired_output / self.produced_output_per_factory
        return self.calc_production_line(needed_factories)

    def calc_production_line(self, needed_factories):
        """Needed ingredient factories, bus belts, and bus pipes"""

        # Get values per output factory
        facts_and_bus = self.material.number_of_ingredient_factories(self.factories,
                                                                     self.bus_belt)
        # Update to needed number of output factories
        upd_facts_and_bus = []
        for i in facts_and_bus:
            upd = {key: val*needed_factories for key, val in i.items()}
            upd_facts_and_bus.append(upd)

        return ProductionLine(needed_factories, *upd_facts_and_bus)

    def printable_production_line(self, production_line):
        out_str = ''
        number_of_main_factories = production_line.main_factory
        out_str += '\n-------------'
        out_str += (f'\n{number_of_main_factories} {self.material} {self.main_factory} produces '
                    f'{self.produced_output_per_second(number_of_main_factories)} units per second')

        for mat, number in production_line.ingredient_factories.items():
            if issubclass(mat, NuclearMaterial):
                out_str += f'\n-- needs {number} of {mat} {self.factories.centrifuge}'
            else:
                out_str += f'\n-- needs {number} of {mat} {self.factories.assembling_machine}'

        for mat, number in production_line.bus_belts.items():
            out_str += f'\n-- needs {number} {self.bus_belt} of {mat} from the bus'

        for mat, number in production_line.bus_pipes.items():
            out_str += f'\n-- needs {number} of {mat} from the bus'

        return out_str


class SmeltingCalculator(object):
    """docstring for SmeltingCalculator."""
    OUTPUT_UNIT = ['belt', 'items']

    def __init__(self, material, output_unit, furnace=StoneFurnace,
                 belt=YellowBelt, fuel=Coal):

        super(SmeltingCalculator, self).__init__()
        self.material = material
        self.furnace = furnace
        self.belt = belt
        self.fuel = fuel
        self.output_unit = output_unit

    @property
    def output_unit(self):
        return self._output_unit

    @output_unit.setter
    def output_unit(self, unit):
        if unit not in self.OUTPUT_UNIT:
            raise ValueError(f'{unit} is not an allow output unit.'
                             f' Must be one of {self.OUTPUT_UNIT}')
        self._output_unit = unit

    def calc_production_line_from_desired_belts(self, desired_belts):
        return self.calc_production_line(desired_belts)

    def calc_production_line_from_desired_output(self, desired_output):
        needed_belts = desired_output / self.belt.capacity
        return self.calc_production_line(needed_belts)

    def calc_production_line(self, output_belts):
        furnaces = self.material.furnaces_per_x_belt(output_belts,
                                                     self.belt,
                                                     self.furnace)

        # Assumes the same input belts as the output belt (self.belt)
        ingredient_belts = {key: val*output_belts for key, val in
                            self.material.ratio_ingredient_output().items()}
        fuel_belts = self.furnace.x_fuel_belts(self.fuel, self.belt) * furnaces

        return SmeltingProductionLine(furnaces, ingredient_belts, fuel_belts)

    def printable_production_line(self, output, production_line):

        (ingredient,
         ingredient_belts) = list(production_line.ingredient_belts.items())[0]

        str = (f'\n-------------\n{production_line.furnaces} {self.material}'
               f' {self.furnace} produces ')

        if self.output_unit == 'belt':
            str += f'{output} {self.belt} of {self.material}'
        elif self.output_unit == 'items':
            str += f'{output} {self.material} units per second'

        str += (f'\n--- needs {ingredient_belts} {self.belt} of {ingredient}'
                f'\n--- needs {production_line.fuel_belts} {self.belt} of {self.fuel}')

        return str


class OilProcessingCalculator():
    """docstring for OilProcessingCalculator."""

    def __init__(self, material, recipe_type, use_optimal_recipe=True):

        super().__init__()
        self.material = material
        self.recipe_type = recipe_type  # TODO: change to type of processing
        self.use_optimal_recipe = use_optimal_recipe

    def calc_production_line_from_desired_output(self, desired_ouput):
        recipe = self.get_desired_recipe()
        # Scale recipe according to desired output
        desire_ratio = desired_ouput / recipe.output[self.material]
        recipe.scale(desire_ratio)

        # Scale all base recipes too
        scaled_ingredients_base_recipes = {}
        scaled_output_base_recipes = {}
        for r, scale in recipe.recipes.items():
            scaled_ingredients_base_recipes[r] = {
                key: val*scale for key, val
                in r.all_ingredient_units_needed_per_second().items()}
            scaled_output_base_recipes[r] = {
                key: val*scale for key, val
                in r.all_output_units_per_second().items()
            }

        return OilProductionLine(recipe, scaled_ingredients_base_recipes,
                                 scaled_output_base_recipes)

    def get_desired_recipe(self):

        rr = self.material.get_recursive_recipes(self.recipe_type)
        if self.use_optimal_recipe:
            return rr[0]

        raise NotImplementedError

    def printable_production_line(self, output, production_line):

        recipe_level = "with the optimal recursive layout" if self.use_optimal_recipe else ''

        str = '\n-------------'
        str += (f'\nProduce {output} {self.material} per second '
                f'using {self.recipe_type} {recipe_level}')

        for mat, val in production_line.recipe.input.items():
            str += (f'\n-- needs total of {val} {mat} per second')

        for mat, val in production_line.recipe.output.items():
            if mat is not self.material:
                str += (f'\n++ produces excess {val} {mat}')

        str += '\n'
        for r, val in production_line.recipe.recipes.items():
            str += (f'\n-- needs total of {val} {r.production_machine} with {r}')
            for ingr, val in production_line.base_ingredient_recipes[r].items():
                str += (f'\n  -- needs {val} {ingr}')
            for out, val in production_line.base_output_recipes[r].items():
                str += (f'\n  ++ produces {val} {out}')
            str += '\n'

        return str
