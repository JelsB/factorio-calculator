from factpy.natural_resources import Stone
from factpy.material_types import SmeltedMaterial, AssembledMaterial
from factpy.bus import BusMaterial
from factpy.natural_resources import IronOre, Water


class StoneBrick(BusMaterial, SmeltedMaterial):
    """Extracts oil from oil fields."""
    crafting_time = 3.2
    ingredients = {Stone: 2}
    output = 1


class Concrete(AssembledMaterial):
    """Serves as path with more advanced bonuses compared with stone bricks."""
    crafting_time = 10
    ingredients = {IronOre: 1, StoneBrick: 5, Water: 100}
    output = 10
