from factpy.material_types import AssembledMaterial
from factpy.terrain import StoneBrick


class Wall(AssembledMaterial):
    """Provides basic damage protection against enemies."""
    crafting_time = 0.5
    ingredients = {StoneBrick: 5}
    output = 1
