from factpy.bus import BusMaterial
from factpy.material_types import MinedMaterial


class IronOre(MinedMaterial):
    """IronOre is a resource found on the map."""


class Coal(BusMaterial, MinedMaterial):
    """Coal is a resource found on the map."""
    fuel_value = 4  # MJ


class Wood(MinedMaterial):
    """Wood is a resource found on the map."""
    fuel_value = 2  # MJ


class Stone(BusMaterial, MinedMaterial):
    """Stone is a resource found on the map."""


class CopperOre(MinedMaterial):
    """CopperOre is a resource found on the map."""


class UraniumOre(BusMaterial, MinedMaterial):
    """UraniumOre is a resource found on the map."""


class RawFish(MinedMaterial):
    """RawFish is a resource found on the map."""


class CrudeOil(MinedMaterial):
    """CrudeOil is a resource found on the map."""


class Water(MinedMaterial):
    """Water is a resource found on the map."""
