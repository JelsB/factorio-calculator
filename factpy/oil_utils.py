from collections import Counter
from dataclasses import dataclass, fields


@dataclass(init=False)
class RecursiveOilRecipe():
    """docstring for RecursiveOilRecipe."""
    # fields
    input: {}
    output: {}
    recipes: {}

    def __init__(self, input, output, recipes):
        self.input = Counter(input)
        self.output = Counter(output)
        self.recipes = Counter(recipes)

    @property
    def level(self):
        return max([recipe.level for recipe in self.recipes.keys()])

    @property
    def field_attributes(self):
        '''returns a list of attributes which are also fields of the instance'''
        return [getattr(self, i.name) for i in fields(self)]

    def scale(self, scale_value):
        '''Scales all attributes values.'''
        for attr in self.field_attributes:
            for key, val in attr.items():
                attr[key] = val * scale_value
