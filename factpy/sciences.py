from factpy.plates import CopperPlate
from factpy.crafting_components import (IronGear, RedCircuit, BlueCircuit,
                                        EngineUnit, FlyingRobotFrame)
from factpy.belts import YellowBelt
from factpy.inserters import BasicInserter
from factpy.material_types import AssembledMaterial
from factpy.weapons import Grenade, PiercingRoundsMagazine
from factpy.defense import Wall
from factpy.oil_products import Sulfur
from factpy.furnaces import ElectricFurnace
from factpy.modules import ProductivityModule
from factpy.transport import Rail
from factpy.rocket_parts import LowDensityStructure


class RedScience(AssembledMaterial):
    """RedScience is first tier of science pack."""
    crafting_time = 5
    ingredients = {CopperPlate: 1, IronGear: 1}
    output = 1


class GreenScience(AssembledMaterial):
    """GreenScience is second tier of science pack."""
    crafting_time = 6
    ingredients = {BasicInserter: 1, YellowBelt: 1}
    output = 1


class BlackScience(AssembledMaterial):
    """BlackScience is third tier of science pack."""
    crafting_time = 10
    ingredients = {Grenade: 1, PiercingRoundsMagazine: 1, Wall: 2}
    output = 2


class BlueScience(AssembledMaterial):
    """BlueScience is fourth tier of science pack."""
    crafting_time = 24
    ingredients = {RedCircuit: 3, EngineUnit: 2, Sulfur: 1}
    output = 2


class PurpleScience(AssembledMaterial):
    """PurpleScience is fifth tier of science pack."""
    crafting_time = 21
    ingredients = {ElectricFurnace: 1, ProductivityModule: 1, Rail: 30}
    output = 3


class GoldScience(AssembledMaterial):
    """GoldScience is sixth tier of science pack."""
    crafting_time = 21
    ingredients = {FlyingRobotFrame: 1, LowDensityStructure: 3, BlueCircuit: 2}
    output = 3


class SpaceScience(AssembledMaterial):
    """SpaceScience is last tier of science pack."""
