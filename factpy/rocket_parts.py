from factpy.material_types import AssembledMaterial
from factpy.plates import CopperPlate, SteelPlate
from factpy.oil_products import PlasticBar, LightOil, SolidFuel


class RocketFuel(AssembledMaterial):
    """Fuel which is also used in the production of rockets."""
    fuel_value = 100  # MJ
    crafting_time = 30
    ingredients = {LightOil: 10, SolidFuel: 10}
    output = 1


class LowDensityStructure(AssembledMaterial):
    """
    Used in the production of modular armor/equipment,
     Utility science packs, and orbital rockets.
     """
    crafting_time = 20
    ingredients = {CopperPlate: 20, PlasticBar: 5, SteelPlate: 2}
    output = 1
