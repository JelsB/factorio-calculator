from factpy.material_types import NuclearMaterial
from factpy.natural_resources import UraniumOre
from factpy.rocket_parts import RocketFuel


class Uranium235(NuclearMaterial):
    """Refined product of uranium ore."""
    crafting_time = 12
    ingredients = {UraniumOre: 10}
    output = 0.007


class Uranium238(NuclearMaterial):
    """Refined product of uranium ore."""
    crafting_time = 12
    ingredients = {UraniumOre: 10}
    output = 0.993


class NuclearFuel(NuclearMaterial):
    """Fuel with the highest energy density."""
    fuel_value = 121e3  # MJ
    crafting_time = 90
    ingredients = {RocketFuel: 1, Uranium235: 1}
    output = 1
