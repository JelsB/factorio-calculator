

class BusMaterial():
    """Material which is produced in bulk (on the bus)."""


class SolidOilBusMaterial(BusMaterial):
    """Material which is produced in bulk (on the bus)."""


class LiquidOilBusMaterial(BusMaterial):
    """Material which is produced in bulk (on the bus)."""
