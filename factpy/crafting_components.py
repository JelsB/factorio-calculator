from factpy.plates import IronPlate, CopperPlate, SteelPlate
from factpy.material_types import AssembledMaterial, MinedMaterial
from factpy.pipes import Pipe
from factpy.oil_products import PlasticBar, Lubricant, SulfuricAcid
from factpy.bus import BusMaterial


class IronGear(AssembledMaterial):
    """IronGear is intermediate product made from iron plates."""
    crafting_time = 0.5
    ingredients = {IronPlate: 2}
    output = 1

    @classmethod
    def a_class_method(cls):
        print("this is a class method")


class CopperWire(AssembledMaterial):
    """CopperWire is intermediate product made from copper plates."""
    crafting_time = 0.5
    ingredients = {CopperPlate: 1}
    output = 2


class IronStick(AssembledMaterial):
    """IronStick is intermediate product made from iron plates."""
    crafting_time = 0.5
    ingredients = {IronPlate: 1}
    output = 2


class GreenCircuit(BusMaterial, AssembledMaterial):
    """GreenCircuit is the first tier of circuits."""
    crafting_time = 0.5
    ingredients = {CopperWire: 3, IronPlate: 1}
    output = 1


class RedCircuit(BusMaterial, AssembledMaterial):
    """RedCircuit is the second tier of circuits."""
    crafting_time = 6
    ingredients = {CopperWire: 4, GreenCircuit: 2, PlasticBar: 2}
    output = 1


class BlueCircuit(BusMaterial, AssembledMaterial):
    """RedCircuit is the third tier of circuits."""
    crafting_time = 10
    ingredients = {RedCircuit: 4, GreenCircuit: 20, SulfuricAcid: 5}
    output = 1


class Steam(MinedMaterial):
    """Is produced by boilers."""


class Battery(AssembledMaterial):
    """Used for building various vehicles."""
    crafting_time = 4
    ingredients = {CopperPlate: 1, IronPlate: 1, SulfuricAcid: 20}
    output = 1


class EngineUnit(AssembledMaterial):
    """Used for building various vehicles."""
    crafting_time = 10
    ingredients = {IronGear: 1, Pipe: 2, SteelPlate: 1}
    output = 1


class ElectricEngineUnit(AssembledMaterial):
    """Used for building various vehicles."""
    crafting_time = 10
    ingredients = {GreenCircuit: 2, EngineUnit: 1, Lubricant: 15}
    output = 1


class FlyingRobotFrame(AssembledMaterial):
    """Used to build logistic and construction robots."""
    crafting_time = 20
    ingredients = {Battery: 2, ElectricEngineUnit: 1, GreenCircuit: 3,
                   SteelPlate: 1}
    output = 1
