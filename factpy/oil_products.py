from factpy.natural_resources import Coal, Water
from factpy.plates import IronPlate
from factpy.bus import BusMaterial, SolidOilBusMaterial, LiquidOilBusMaterial
from factpy.material_types import OilProccessedMaterial


class Petroleum(LiquidOilBusMaterial, OilProccessedMaterial):
    """Petroleum gas is used to create plastic bars and sulfur."""
    level = 0


class LightOil(LiquidOilBusMaterial, OilProccessedMaterial):
    """Petroleum gas is used to create plastic bars and sulfur."""
    level = 1


class HeavyOil(LiquidOilBusMaterial, OilProccessedMaterial):
    """Petroleum gas is used to create plastic bars and sulfur."""
    level = 2


class Lubricant(LiquidOilBusMaterial, OilProccessedMaterial):
    """Used in the production of express transport belts and their counterparts."""
    # TODO: Add recipe
    level = 3


class Sulfur(SolidOilBusMaterial):
    """Sulfur component in the production of Chemical science pack,
     Sulfuric acid and Explosives.
     """
    crafting_time = 1
    ingredients = {Petroleum: 30, Water: 30}
    output = 2


class SulfuricAcid(BusMaterial):
    """SulfuricAcid is liquid that is used to create batteries and
     processing units.
     """
    crafting_time = 1
    ingredients = {IronPlate: 1, Sulfur: 5, Water: 100}
    output = 50


class PlasticBar(SolidOilBusMaterial):
    """PlasticBar are a requirement for the production of red circuits."""
    crafting_time = 1
    ingredients = {Coal: 1, Petroleum: 20}
    output = 2


class SolidFuel(SolidOilBusMaterial, OilProccessedMaterial):
    """Kind of fuel made in a chemical plant."""
    fuel_value = 12  # MJ
