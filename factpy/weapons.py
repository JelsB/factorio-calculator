from factpy.material_types import AssembledMaterial
from factpy.natural_resources import Coal
from factpy.plates import IronPlate, CopperPlate, SteelPlate
from factpy.nuclear import Uranium238


class Grenade(AssembledMaterial):
    """
    A basic explosive weapon that is throw-able within a short distance,
    dealing damage in a small area of effect.
    """
    crafting_time = 8
    ingredients = {Coal: 10, IronPlate: 5}
    output = 1


class FireArmMagazine(AssembledMaterial):
    """RedScience is first tier of science pack."""
    crafting_time = 1
    ingredients = {IronPlate: 4}
    output = 1


class PiercingRoundsMagazine(AssembledMaterial):
    """A more advanced ammunition type for gun weapons."""
    crafting_time = 3
    ingredients = {CopperPlate: 5, FireArmMagazine: 1, SteelPlate: 1}
    output = 1


class UraniumRoundsMagazine(AssembledMaterial):
    """Most advanced ammunition type for gun weapons."""
    crafting_time = 10
    ingredients = {PiercingRoundsMagazine: 1, Uranium238: 1}
    output = 1
