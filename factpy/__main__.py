from factpy.calculators import FactoryCalculator, SmeltingCalculator, OilProcessingCalculator
from factpy.assembling_machines import AssemblingMachine1
from factpy.sciences import GreenScience, BlackScience
from factpy.belts import YellowBelt, RedBelt

from factpy.furnaces import StoneFurnace, SteelFurnace
from factpy.plates import IronPlate, CopperPlate, SteelPlate
from factpy.inserters import BurnerInserter, FastInserter
from factpy.crafting_components import IronGear
from factpy.crafting_components import GreenCircuit
from factpy.natural_resources import Coal
from factpy.oil_products import Petroleum
from factpy.material_types import AdvancedOilProcessingRecipe
# from factpy.oil_processing import (Petroleum, LightOil, BasicOilProcessing,
#                                    AdvancedOilProcessing, CoalLiquefaction,
#                                    LightOilCracking, HeavyOilCracking, HeavyOil)

# NOTE: this import is needed to initialise all subclasses of oil processing recipes
# TODO: fix this...
import factpy.oil_processing


def run():
    wanted_material = BlackScience
    wanted_number_of_factories = 4
    wanted_number_of_output_per_second = 1/3
    assembling_machine = AssemblingMachine1
    bus_belt = YellowBelt

    calc = FactoryCalculator(wanted_material, assembling_machine, bus_belt)

    production_line = calc.calc_production_line_from_desired_factories(
        wanted_number_of_factories)
    str = calc.printable_production_line(production_line)
    print(str)
    # ------------------------------

    production_line = calc.calc_production_line_from_desired_output(
        wanted_number_of_output_per_second)
    str = calc.printable_production_line(production_line)
    print(str)
    # ------------------------------
    # ------------------------------

    wanted_material = IronPlate
    wanted_number_of_belt = 2
    output_unit = 'belt'
    belt = YellowBelt
    furnace = StoneFurnace
    fuel = Coal

    calc = SmeltingCalculator(wanted_material, output_unit, furnace, belt, fuel)
    production_line = calc.calc_production_line_from_desired_belts(
        wanted_number_of_belt)

    str = calc.printable_production_line(wanted_number_of_belt, production_line)
    print(str)
    # ------------------------------

    wanted_number_of_output_per_second = 30
    calc.output_unit = 'items'
    production_line = calc.calc_production_line_from_desired_output(
        wanted_number_of_output_per_second)
    str = calc.printable_production_line(wanted_number_of_output_per_second,
                                         production_line)
    print(str)
    # ------------------------------
    # ------------------------------

    wanted_material = Petroleum
    desired_ouput = 19.5
    recipe_type = AdvancedOilProcessingRecipe

    calc = OilProcessingCalculator(wanted_material, recipe_type)

    production_line = calc.calc_production_line_from_desired_output(desired_ouput)
    str = calc.printable_production_line(desired_ouput, production_line)
    print(str)


if __name__ == '__main__':
    run()
