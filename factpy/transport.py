from factpy.material_types import AssembledMaterial
from factpy.crafting_components import IronStick
from factpy.plates import SteelPlate
from factpy.terrain import Stone


class Rail(AssembledMaterial):
    """Used for building a railway track for trains."""
    crafting_time = 0.5
    ingredients = {IronStick: 1, SteelPlate: 1, Stone: 1}
    output = 2
