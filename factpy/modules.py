from factpy.crafting_components import GreenCircuit, RedCircuit

from factpy.material_types import AssembledMaterial


class SpeedModule(AssembledMaterial):
    """Increases a machine's speed by 20% and its energy consumption by 50%."""
    crafting_time = 15
    ingredients = {RedCircuit: 5, GreenCircuit: 5}
    output = 1


class ProductivityModule(AssembledMaterial):
    """
     Add a second purple "production bar" to item producing buildings and labs.
     It fills by 4% per crafting cycle for this tier of module. When the
     production bar reaches 100% one extra set of outputs is produced.
    """
    crafting_time = 15
    ingredients = {RedCircuit: 5, GreenCircuit: 5}
    output = 1
