from factpy.crafting_components import IronGear, Steam
from factpy.crafting_components import GreenCircuit
from factpy.pipes import Pipe
from factpy.material_types import (AssembledMaterial, BasicOilProcessingRecipe,
                                   AdvancedOilProcessingRecipe,
                                   CoalOilProcessingRecipe)
from factpy.plates import SteelPlate
from factpy.terrain import StoneBrick

from factpy.natural_resources import CrudeOil, Water, Coal
from factpy.oil_products import Petroleum, LightOil, HeavyOil


class OilRefinery(AssembledMaterial):
    """Extracts oil from oil fields."""
    crafting_time = 8
    ingredients = {GreenCircuit: 10, IronGear: 10, Pipe: 10, SteelPlate: 15,
                   StoneBrick: 10}
    output = 1


class ChemicalPlant(AssembledMaterial):
    """Extracts oil from oil fields."""
    crafting_time = 5
    ingredients = {GreenCircuit: 5, IronGear: 5, Pipe: 5, SteelPlate: 5}
    output = 1


class BasicOilProcessing(BasicOilProcessingRecipe):
    """A basic oil processing recipe."""
    crafting_time = 5
    ingredients = {CrudeOil: 100}
    output = {Petroleum: 45}
    production_machine = OilRefinery


class AdvancedOilProcessing(AdvancedOilProcessingRecipe):
    """An advanced oil processing recipe."""
    crafting_time = 5
    ingredients = {CrudeOil: 100, Water: 50}
    output = {Petroleum: 55, LightOil: 45, HeavyOil: 25}
    production_machine = OilRefinery


class LightOilCracking(AdvancedOilProcessingRecipe):
    """An advanced oil processing recipe."""
    crafting_time = 2
    ingredients = {LightOil: 30, Water: 30}
    output = {Petroleum: 20}
    production_machine = ChemicalPlant


class HeavyOilCracking(AdvancedOilProcessingRecipe):
    """An advanced oil processing recipe."""
    crafting_time = 2
    ingredients = {HeavyOil: 40, Water: 30}
    output = {LightOil: 30}
    production_machine = ChemicalPlant


class CoalLiquefaction(CoalOilProcessingRecipe):
    """A coal coal liquefaction processing recipe."""
    crafting_time = 5
    ingredients = {Coal: 10, HeavyOil: 25, Steam: 50}
    output = {Petroleum: 10, HeavyOil: 90, LightOil: 20}
    production_machine = OilRefinery
