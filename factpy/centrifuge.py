from factpy.crafting_components import RedCircuit, IronGear
from factpy.plates import SteelPlate
from factpy.terrain import Concrete
from factpy.material_types import AssembledMaterial


class Centrifuge(AssembledMaterial):
    """AssemblingMachine1 is the most basic assembling machine."""
    crafting_speed = 1
    crafting_time = 4
    ingredients = {RedCircuit: 100, Concrete: 100, IronGear: 100, SteelPlate: 50}
    output = 1
