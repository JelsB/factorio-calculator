# Factorio calculator #

This calculator is intended to easily get factory ratios when playing [Factorio](https://factorio.com/). This calculator is based on version 0.18 of the game. This repo is currently in its initial state and is still under development.

The idea behind this calculator is to give you useful ratios and not all the way down to ore. E.g. materials produced in bulk (on the bus) are considered a raw product. Required input materials will be given in units of belts rather than number of material.


## How you can use it at the moment: ###
### Materials produced in assembling machines ###
Adapt the desired parameters in the *factpy/\_\_main__.py*
```python
wanted_material = GreenScience
wanted_number_of_factories = 4
wanted_number_of_output_per_second = 1/3
assembling_machine = AssemblingMachine1  # default
bus_belt = YellowBelt  # default
```
If you want to calculate what resources you need based on the number of desired factories, add this to *factpy/\_\_main__.py*
```python
production_line = calc.calc_production_line_from_desired_factories(wanted_number_of_factories)
str = calc.printable_production_line(production_line)
print(str)
```
If you want to calculate what resources you need based on a desired number of output units per second, add this to *factpy/\_\_main__.py*
```python
production_line = calc.calc_production_line_from_desired_output(wanted_number_of_output_per_second)
str = calc.printable_production_line(production_line)
print(str)
```
Run the package
```bash
python factpy
```
which will print something like this:
```shell
4 Green Science Assembling Machine1 produces 0.3333333333333333 units per second
-- needs 0.3333333333333333 of Basic Inserter Assembling Machine1
-- needs 0.5 of Iron Gear Assembling Machine1
-- needs 0.16666666666666666 of Yellow Belt Assembling Machine1
-- needs 0.02222222222222222 Yellow Belt of Green Circuit from the bus
-- needs 0.05555555555555555 Yellow Belt of Iron Plate from the bus
```


### Materials produced in furnaces ###
Adapt the desired parameters in the *factpy/\_\_main__.py*
```python
wanted_material = IronPlate
wanted_number_of_belt = 2
output_unit = 'belt'
belt = YellowBelt  # default
furnace = StoneFurnace  # default
fuel = Coal  # default
```
If you want to calculate what resources you need based on the number of desired output belts, add this to *factpy/\_\_main__.py*
```python
calc = SmeltingCalculator(wanted_material, output_unit, furnace, belt, fuel)
production_line = calc.calc_production_line_from_desired_belts(wanted_number_of_belt)

str = calc.printable_production_line(wanted_number_of_belt, production_line)
print(str)
```
If you want to calculate what resources you need based on the number of desired output units per second, add this to *factpy/\_\_main__.py*
```python
wanted_number_of_output_per_second = 30
calc.output_unit = 'items'
production_line = calc.calc_production_line_from_desired_output(wanted_number_of_output_per_second)
str = calc.printable_production_line(wanted_number_of_output_per_second, production_line)
print(str)
```
Run the main file
```Bash
python factpy
```

which will print something like this:
```shell
96.0 Iron Plate Stone Furnace produces 2 Yellow Belt of Iron Plate
--- needs 2.0 Yellow Belt of Iron Ore
--- needs 0.14400000000000002 Yellow Belt of Coal
```
### Material produced by oil processing ###
_(no coal liquefaction yet)_

Adapt the desired parameters in the *factpy/\_\_main__.py*

```python
wanted_material = Petroleum
desired_ouput = 19.5
recipe_type = AdvancedOilProcessingRecipe
```
If you want to calculate what resources you need based on the number of desired output units per second, add this to *factpy/\_\_main__.py*
```python
calc = OilProcessingCalculator(wanted_material, recipe_type)

production_line = calc.calc_production_line_from_desired_output(desired_ouput)
str = calc.printable_production_line(desired_ouput, production_line)
print(str)

```
Run the package
```bash
python factpy
```
which will print something like this:
```shell
Produce 19.5 Petroleum per second using Advanced Oil Processing Recipe with the optimal recursive layout
-- needs total of 20.0 Crude Oil per second
-- needs total of 26.5 Water per second


-- needs total of 1.0 Oil Refinery with Advanced Oil Processing
  -- needs 20.0 Crude Oil
  -- needs 10.0 Water
  ++ produces 11.0 Petroleum
  ++ produces 9.0 Light Oil
  ++ produces 5.0 Heavy Oil


-- needs total of 0.85 Chemical Plant with Light Oil Cracking
  -- needs 12.75 Light Oil
  -- needs 12.75 Water
  ++ produces 8.5 Petroleum


-- needs total of 0.25 Chemical Plant with Heavy Oil Cracking
  -- needs 5.0 Heavy Oil
  -- needs 3.75 Water
  ++ produces 3.75 Light Oil
```
**NOTE:** by default only the optimal recursive layout of factories is shown. It is possible to calculate less optimal layouts (less output of wanted material) but displaying this is still under development.

### Materials produced in Centrifuge ###
```python
wanted_material = Uranium238
wanted_number_of_output_per_second = 1

calc = FactoryCalculator(wanted_material)

production_line = calc.calc_production_line_from_desired_output(wanted_number_of_output_per_second)
str = calc.printable_production_line(production_line)
print(str)
```
produces something like
```shell
12.084592145015105 Uranium238 Centrifuge produces 1.0 units per second
-- needs 0.3356831151393085 Yellow Belt of Uranium Ore from the bus
```

### Materials produced in mixed factories ###
```python
wanted_material =  NuclearFuel
wanted_number_of_output_per_second = 1

calc = FactoryCalculator(wanted_material)

production_line = calc.calc_production_line_from_desired_output(wanted_number_of_output_per_second)
str = calc.printable_production_line(production_line)
print(str)
```
produces something like
```shell
90.0 Nuclear Fuel Centrifuge produces 1.0 units per second
-- needs 30.000000000000004 of Rocket Fuel Assembling Machine1
-- needs 1714.2857142857142 of Uranium235 Centrifuge
-- needs 0.33333333333333337 Yellow Belt of Solid Fuel from the bus
-- needs 47.61904761904761 Yellow Belt of Uranium Ore from the bus
-- needs 5.0 of Light Oil from the bus
```


## Future plans ##
* Make more userfriedly to run
* ~~Adapt smelting to have similar feature as assembling~~
* ~~Add number of needed belts to material coming from the bus~~
* Able to specify you own bus materials
* Add UI
* ~~Add nuclear calculations~~
* Add uranium enrichment
* ~~Add oil calculations~~
* Add coal liquefaction
* ...

## Requirements ##
python 3.7 or above
